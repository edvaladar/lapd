module.exports = function(sequelize, DataTypes) {
    return sequelize.define('User', {
        avatar: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING(64)
        },
        username: {
            type: DataTypes.STRING
        },
        nickname: {
            type: DataTypes.STRING
        },
        connected: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        facebookCredentials: {
            type: DataTypes.JSONB
        }
    });
};

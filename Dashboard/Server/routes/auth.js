'use strict';

var Config = require('config');
var _ = require('lodash');
var Joi = require('joi');
var Boom = require('boom');
var Moment = require('moment');
var Jwt = require('jsonwebtoken');
var Models = require('../models');

module.exports = function(server) {
	
    function newToken(user) {
        return Jwt.sign({
                id: user.id,
                expiryDate: Moment().add(7, 'day'),
        }, Config.jwt);
    }
    
	function validate(decodedToken, callback) {
        var error;
        console.log("TOKEN:", decodedToken);
        Models.User.findOne({
            where: {
                id: decodedToken.id
            }
        })
        .then(function (user) {
            return callback(error, true, user.dataValues);
        })
        .catch(function () {
            return callback(error, false, {});
        });
    }
	
    server.method('create_token', newToken, { callback: true });
    
    server.register(require('hapi-auth-jwt'), function () {
    	server.auth.strategy('token', 'jwt', {
       		key: Config.jwt,
        	validateFunc: validate
    	});
    });
	
	server.route({
		method: 'POST',
		path: '/api/login',
		config: {
            tags: ['api'],
            validate: {
                payload: {
                    username: Joi.string().required(),
                    password: Joi.string().max(64).required()
                }
            },
			handler: function(request, reply) {
                Models.User.findOne({
                    where: {
                        username: request.payload.username,
                        password: request.payload.password
                    }
                })
                .then(function(user) {
                    if (!user)
                        return reply(Boom.unauthorized('No user found'));
                        
                    var ret = _.omit(user.dataValues, 'password');
                    ret.access_token = server.methods.create_token(user.dataValues);
                    reply(ret);
                })
                .catch(function(error) {
                    console.log(error);
                    reply(Boom.badImplementation('Internal server error'));                    
                });
			}
		}
	});
    
    server.route({
        method: 'POST',
        path: '/api/register',
        config: {
            tags: ['api'],
            validate: {
                payload: {
                    username: Joi.string().required(),
                    nickname: Joi.string().required(),
                    password: Joi.string().max(64).required()
                }
            },
            handler: function(request, reply) {
                Models.User.findOne({
                    where: {
                        username: request.payload.username
                    }
                })
                .then(function(user) {
                    if (user)
                        return reply(Boom.badRequest('Username already exists'));
                    
                    Models.User.create({
                        username: request.payload.username,
                        nickname: request.payload.nickname,
                        password: request.payload.password
                    })
                    .then(function(newUser) {
                        var ret = _.omit(newUser.dataValues, 'password');
                        ret.access_token = server.methods.create_token(newUser.dataValues);
                        reply(ret);
                    })
                    .catch(function(error) {
                        console.log(error);
                        reply(Boom.badImplementation('Internal server error'));
                    });
                });
                
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/validateToken',
        config: {
            auth: 'token',
            handler: function(request, reply) {
                reply().code(200);
            }
        }
    });

};

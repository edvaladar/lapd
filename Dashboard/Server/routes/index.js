'use strict';

module.exports = function(server) {
    //require('./auth')(server);
    require('./truck')(server);
    require('./delivery')(server);
    require('./notification')(server);
    // Serve static files
    server.route({
        method: 'GET',
        path: '/{name*}',
        handler: {
            directory: {
                path: '../Client',
                redirectToSlash: true,
                index: true
        }
    }
    });
};
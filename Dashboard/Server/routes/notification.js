'use strict';

var Config = require('config');
var _ = require('lodash');
var Joi = require('joi');
var Boom = require('boom');
var Moment = require('moment');
var Jwt = require('jsonwebtoken');
var Models = require('../models');
var globals = require('../globals.js');
var request = require('request');
var parseString = require('xml2js').parseString;


module.exports = function (server) {

    server.route({
        method: 'POST',
        path: '/api/truck/notifications',
        config: {
            tags: ['api'],
            validate: {
                payload: {
                    vehicleID: Joi.string().required()
                }
            },
            handler: function (req, reply) {
                request(globals.exist_domain + "notifications?id=" + req.payload.vehicleID, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        parseString(body, function (err, result) {
                            reply(result);
                        });
                    }
                    else
                        return reply(Boom.badRequest('No query avaliable'));
                });
            }
        }
    });

};
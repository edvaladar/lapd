'use strict';

var Config = require('config');
var _ = require('lodash');
var Joi = require('joi');
var Boom = require('boom');
var Moment = require('moment');
var Jwt = require('jsonwebtoken');
var Models = require('../models');
var globals = require('../globals.js');
var request = require('request');
var parseString = require('xml2js').parseString;

module.exports = function (server) {

    server.route({
        method: 'GET',
        path: '/api/trucks',
        config: {
            tags: ['api'],
            validate: {
                params: {
                }
            },
            handler: function (req, reply) {
                request(globals.exist_domain + "trucks", function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        parseString(body, function (err, result) {
                            reply(result);
                        });
                    }
                    else
                        return reply(Boom.badRequest('No query avaliable'));
                });
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/api/truck/{vehicleID}',
        config: {
            tags: ['api'],
            validate: {
                params: {
                    vehicleID: Joi.string().required()
                }
            },
            handler: function (req, reply) {
                request(globals.exist_domain + "truck?ID=" + req.params.vehicleID, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        parseString(body, function (err, result) {
                            reply(result);
                        });
                    }
                    else
                        return reply(Boom.badRequest('No query avaliable'));
                });
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/routes',
        config: {
            tags: ['api'],
            validate: {
                payload: {
                    plate: Joi.string().required()
                }
            },
            handler: function (req, reply) {
                request(globals.exist_domain + "routes?plate=" + req.payload.plate, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        parseString(body, function (err, result) {
                            console.dir(result);
                            reply(result);
                        });
                    }
                    else
                        return reply(Boom.badRequest('No query avaliable'));
                });
            }
        }
    });

};
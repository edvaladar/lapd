'use strict';

const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');
var Joi = require('joi');
//var exist = require('./exist.js');

// Create a server with a host and port
var server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, '../Client')
            }
        }
    }
});
server.connection({
    port: 3000
});

server.register(Inert, () => {});

// Plugins
/*server.register({
    register: require('hapi-swagger'),
    options: {
        apiVersion: require('./package').version
    }
}, function (err) {
    if (err) {
        server.log(['error'], 'hapi-swagger load error: ' + err);
    } else {
        server.log(['start'], 'hapi-swagger interface loaded');
    }
});*/


require('./routes')(server);

// Start the server
server.start((err) => {

    if (err) {
        throw err;
    }

    console.log('Server running at:', server.info.uri);
});
angular.module('app.controllers', [])
.controller('DashCtrl', function ($scope, truckService, deliveryService) {
	$('#sidebar').show();
	$scope.trucks = [];
	$scope.deliveries = {
		list:[],
		pending_deliveries: 0,
		pending_orders: 0
	};

	deliveryService.getDeliveries()
	.then(function(result){
		$scope.deliveries.list = result.Deliveries.Delivery;
		console.log($scope.deliveries);
	});

	truckService.getTrucks()
	.then(function (result) {
		$scope.trucks = result.Trucks.Truck;
	});
})

.controller('VehicleCtrl', function ($scope, $stateParams, truckService, deliveryService) {
	$('#sidebar').show();
	$scope.truck = "";
	$scope.deliveries = "";

	truckService.getTruck($stateParams.vehicleID)
	.then(function(_truck){
		$scope.truck = _truck.Truck;

		deliveryService.getTruckDeliveries($scope.truck.Plate[0])
		.then(function(_deliveries) {
			$scope.deliveries = _deliveries.Deliveries.Delivery;
		});
	});

	$scope.map = function(id) {
		$("#"+id).empty();
		var platform = new H.service.Platform({
			'app_id': 'e45E3wpZki93BfGsZg0X',
			'app_code': '5HBIstSJtjVVQTzflU4Rkw'
		});

		var targetElement = document.getElementById(id);

		var defaultLayers = platform.createDefaultLayers();

		var map = new H.Map(
			targetElement,
			defaultLayers.normal.map,
			{
				zoom: 10,
				center: { lat: 52.51, lng: 13.4 }
			});

		var ui = H.ui.UI.createDefault(map, defaultLayers);

		var routingParameters = {
			'mode': 'fastest;car',
			'representation': 'display'
		};

		for (var i = 0; i < $scope.deliveries.length; i++) {
			var path = $scope.deliveries[i].Path[0];

			routingParameters['waypoint0'] = 'geo!' + path.Origin[0].$.lat + ',' + path.Origin[0].$.log;
			var waypointIterator = 1;
			while (waypointIterator < path.Checkpoints[0].Point.length+1) {
				var key = 'waypoint' + waypointIterator;
				routingParameters[key] = 'geo!' + path.Checkpoints[0].Point[waypointIterator-1].$.lat + ',' + path.Checkpoints[0].Point[waypointIterator-1].$.log;
				waypointIterator++;
			};
			routingParameters['waypoint' + waypointIterator] = 'geo!' + path.Destiny[0].$.lat + ',' + path.Destiny[0].$.log;
			console.log(routingParameters);
		}


		var onResult = function(result) {
			var route,
			routeShape,
			strip;
			if(result.response.route) {
				route = result.response.route[0];
				routeShape = route.shape;

				strip = new H.geo.Strip();

				routeShape.forEach(function(point) {
				var parts = point.split(',');
				strip.pushLatLngAlt(parts[0], parts[1]);
				});

				var routeLine = new H.map.Polyline(strip, {
					style: { strokeColor: 'blue', lineWidth: 10 }
				});

				var objects = [routeLine];
				for (var i = 0; i < route.waypoint.length; i++) {
					var marker = new H.map.Marker({
						lat: route.waypoint[i].mappedPosition.latitude,
						lng: route.waypoint[i].mappedPosition.longitude
					});
					objects.push(marker);
				}

				map.addObjects(objects);

				map.setViewBounds(routeLine.getBounds());
			}
		};

		var router = platform.getRoutingService();

		router.calculateRoute(routingParameters, onResult,
			function(error) {
				alert(error.message);
		});
	};
})

.controller('NotificationCtrl', function($scope, $stateParams, truckService, notificationService) {
	$('#sidebar').show();
	$scope.truck = "";
	$scope.notifications = "";

	truckService.getTruck($stateParams.vehicleID)
	.then(function(_truck) {
		$scope.truck = _truck.Truck;

		notificationService.getNotifications($stateParams.vehicleID)
		.then(function(_notifications) {
			$scope.notifications = _notifications.Truck.Notification;
			console.log($scope.notifications);
		});
	});
})

.controller('ErrorCtrl', function ($scope, truckService) {
	$('#sidebar').hide();
});

var app = angular.module('app', ['app.controllers', 'app.services', 'ui.router'])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('dashboard', {
      url: '/dashboard',
      views: {
        nav_top: {
          templateUrl: 'templates/navbar_top.html'
        },
        content: {
          templateUrl: 'templates/dashboard.html',
          controller: 'DashCtrl'
        }
      }
    })
    .state('vehicle', {
      url: '/vehicle/:vehicleID',
      views: {
        nav_top: {
          templateUrl: 'templates/navbar_top.html'
        },
        content: {
          templateUrl: 'templates/vehicle.html',
          controller: 'VehicleCtrl'
        }
      }
    })
    .state('notifications', {
      url: '/notifications/:vehicleID',
      views: {
        nav_top: {
          templateUrl: 'templates/navbar_top.html'
        },
        content: {
          templateUrl: 'templates/notification.html',
          controller: 'NotificationCtrl'
        }
      }
    })
    .state('error', {
      url: '/error',
      views: {
        content: {
          templateUrl: 'templates/error.html',
          controller: 'ErrorCtrl'
        }
      }
    });
  $urlRouterProvider.otherwise('/error');
});



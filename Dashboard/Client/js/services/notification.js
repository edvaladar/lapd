'use strict';

app.service('notificationService', ['ServerConfig', '$q', '$http',
        function (ServerConfig, $q, $http) {

            this.getNotifications = function (vehicleID) {
                var deferred = $q.defer();

                $http.post('/api/truck/notifications', {
                    vehicleID: vehicleID
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };

        }]
);
'use strict';

app.service('truckService', ['ServerConfig', '$q', '$http',
        function (ServerConfig, $q, $http) {

            this.getTrucks = function () {
                var deferred = $q.defer();

                $http.get('/api/trucks')
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.getTruck = function (vehicleID) {
                var deferred = $q.defer();

                $http.get('/api/truck/' + vehicleID)
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.getRoutes = function (plate) {
                var deferred = $q.defer();

                $http.post('/api/routes', {
                    plate: plate
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };
        }]
);
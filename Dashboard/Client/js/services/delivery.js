'use strict';

app.service('deliveryService', ['ServerConfig', '$q', '$http',
        function (ServerConfig, $q, $http) {

            this.getDeliveries = function () {
                var deferred = $q.defer();

                $http.get('/api/deliveries')
                    .then(function (result) {
                        deferred.resolve(result.data);
                    }, function (error) {
                        deferred.reject(error.data.message);
                    });

                return deferred.promise;
            };

            this.getTruckDeliveries = function (plate) {
                var deferred = $q.defer();

                $http.post('/api/truck/deliveries', {
                    plate: plate
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error.data.message);
                });

                return deferred.promise;
            };
        }]
);
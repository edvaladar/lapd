var platform = new H.service.Platform({
  'app_id': 'PNQkiOpXaIYFkVtmwOnk',
  'app_code': 'CYpOad7wkk8rBoIXxA1wqw'
});

var targetElement = document.getElementById('mapContainer');

var defaultLayers = platform.createDefaultLayers();

var map = new H.Map(
  document.getElementById('mapContainer'),
  defaultLayers.normal.map,
  {
    zoom: 10,
    center: { lat: 52.51, lng: 13.4 }
  });

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
var ui = H.ui.UI.createDefault(map, defaultLayers);

var routingParameters = {
  'mode': 'fastest;car',
  'waypoint0': 'geo!50.1120423728813,8.68340740740811',
  'waypoint1': 'geo!52.5309916298853,13.3846220493377',
  'waypoint2': 'geo!53.0000000000000,13.3000000000000',
  'representation': 'display'
};

var onResult = function(result) {
  var route,
  routeShape,
  strip;
  if(result.response.route) {
    route = result.response.route[0];
    routeShape = route.shape;

    strip = new H.geo.Strip();

    routeShape.forEach(function(point) {
      var parts = point.split(',');
      strip.pushLatLngAlt(parts[0], parts[1]);
    });

    var routeLine = new H.map.Polyline(strip, {
      style: { strokeColor: 'blue', lineWidth: 10 }
    });

    var objects = [routeLine];
    for (var i = 0; i < route.waypoint.length; i++) {
      var marker = new H.map.Marker({
        lat: route.waypoint[i].mappedPosition.latitude,
        lng: route.waypoint[i].mappedPosition.longitude
      });
      objects.push(marker);
    }

    map.addObjects(objects);

    map.setViewBounds(routeLine.getBounds());
  }
};

var router = platform.getRoutingService();

router.calculateRoute(routingParameters, onResult,
  function(error) {
    alert(error.message);
  }
);
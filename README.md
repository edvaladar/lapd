# LAPD #
## Functional Requirements ##
| ID | Description | Priority |
|---|---|---|
| R1.1 | Como manager, quero definir a rota do camião. |  |
| R1.2 | Como manager, quero actualizar uma rota. |   |
| R1.3 | Como manager, quero ver os camiões activos. |   |
| R1.4 | Como manager, quero enviar uma notificação a um camião. |  |
| R1.5 | Como manager, quero ver o histórico de rotas. |  |
| R1.6 | Como manager, quero inserir um novo veículo. |  |
| R2.1 | Como driver, quero visualizar o mapa com a rota estabelecida. |  |
| R2.2 | Como driver, quero receber notificações e rotas. |  |
| R |  |  |
| R |  |  |
| R |  |  |
| R |  |  |
| R |  |  |

## Non-functional Requirements ##
| ID | Description |
|---|---|---|
| R2.1 | O sistema deve permanecer funcional em qualquer navegador. |
| R2.3 | Deve ser usado o sistema de gestão de bases de dados PostgreSQL 9.5.2. e uma base de dados nativa XML exist-db 2.2. |
| R2.2 | O sistema deve ser simples e fácil de usar. |
| R2.4 | O sistema deve estar preparado para suportar o povoamento das bases de dados. |
| R2.5 | O sistema deve respeitar os princípios éticos no desenvolvimento de software. |